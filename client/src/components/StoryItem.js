import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import moment from 'moment';

import { startRemoveStory } from '../actions/stories';

export const StoryItem = ({ id, title, story, createdAt, startRemoveStory }) => {

  return (
    <div className="story">
      <Link className="story--title" to={`/read/${id}`}><h3>{title}</h3></Link>
      <p className="story--body">{story}</p>
      <div className="story--misc">
        <span className="story--time">{moment(createdAt).fromNow()}</span>
        <button className="button button--detele" onClick={() => startRemoveStory({ id })}><i className="fas fa-trash"></i></button>
      </div>
    </div>
  )
}

const mapDispatchToProps = (dispatch) => ({
  startRemoveStory: ({ id }) => dispatch(startRemoveStory({ id }))
});

export default connect(undefined, mapDispatchToProps)(StoryItem)