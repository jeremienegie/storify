import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import moment from 'moment';

import { startFetchStory, startRemoveStory } from '../actions/stories';
import CommentForm from '../components/comment/CommentForm';
import ShowComments from '../components/comment/ShowComment';

export class ReadStory extends React.Component {
  doesStoryExist = () => {
    const id = this.props.match.params.id;
    if (!this.props.startFetchStory(id).length) {
      return (
        <div className="nothing-to-show">Ooops! that story doesn't exist</div>
      )
    }

    return this.props.startFetchStory(id).map((story) => (
      <div className="story" key={story.id}>
        <Link className="story--title" to={`/edit/${story.id}`}>Edit</Link>
        <h3>{story.title}</h3>
        <p className="story--body">{story.story}</p>
        <div className="story--misc">
          <span className="story--time">{moment(story.createdAt).fromNow()}</span>
          <button className="button button--detele" onClick={() => this.props.startRemoveStory(story.id)}><i className="fas fa-trash"></i></button>
        </div>
        <span className="story--dummy-span"></span>
        <div>
          {story.bookmarked ?
            (
              <div className="nothing-to-show">
                <p>Commenting is not available for bookmarked story</p>
              </div>
            ) : (
              <div>
                <div>
                  <ShowComments storyId={this.props.match.params.id} />
                </div>
                <div>
                  <CommentForm storyId={this.props.match.params.id} />
                </div>
              </div>
            )
          }
        </div>
      </div>
    ));
  }
  render() {
    return (
      <div className="content-container">
        <Link to='/story'><i className="fas fa-arrow-left"></i></Link>
        <div>
          {this.doesStoryExist()}
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  startFetchStory: (id) => dispatch(startFetchStory(id)),
  startRemoveStory: (id) => dispatch(startRemoveStory({ id }))
});

export default connect(undefined, mapDispatchToProps)(ReadStory);