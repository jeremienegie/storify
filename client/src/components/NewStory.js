import React from 'react';
import { connect } from 'react-redux';

import StoryForm from './StoryForm';
import { startAddStory } from '../actions/stories';

export class NewStory extends React.Component {

  onSubmit = (story) => {
    this.props.startAddStory({
      createdAt: Date.now(),
      ...story
    });
    this.props.history.push('/story');
  }

  render() {
    return (
      <div className="content-container">
        <StoryForm onSubmit={this.onSubmit}/>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  startAddStory: (story) => dispatch(startAddStory(story))
});

export default connect(undefined, mapDispatchToProps)(NewStory);