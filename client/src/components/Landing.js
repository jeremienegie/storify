import React from 'react';
import { connect } from 'react-redux';

import Login from './Login';
import Story from './Story';

const Landing = ({ isAuthenticated }) => (
  <div>
    {isAuthenticated ? <Story /> : <Login />}
  </div>
);

const mapStateToProps = ({ auth }) => ({
  isAuthenticated: !!auth.uid
});

export default connect(mapStateToProps)(Landing);