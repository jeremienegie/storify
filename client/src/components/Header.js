import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { startLogout } from '../actions/auth/signin';

export class Header extends React.Component {
  render() {
    return (
      <header className="header">
      <div className="content-container">
        <div className="header__content">
          <div>
            <Link to="/" className="header__title">
              <h1 className="header__logo">Storify</h1>
            </Link>
          </div>
          {this.props.isAuthenticated &&
          <div className="header__menu">
            <div className="header__icon-link">
              <Link className="header__link header__link--round" to='/bookmarks'><i className="fas fa-bookmark"></i></Link>
              <Link className="header__link header__link--round" to='/new'><i className="fas fa-edit"></i></Link>
            </div>
            <span>Hi {this.props.userInfo.firstname}</span>
            <button className="button button--link-simple button--secondary" onClick={this.props.startLogout}>Log out</button>
          </div>
          }
        </div>
      </div>
    </header>
    );
  }
}

const mapStateToProps = (state) => ({
  isAuthenticated: !!state.auth.uid,
  userInfo: state.auth.userInfo
});

const mapDispatchToProps = (dispatch) => ({
  startLogout: () => dispatch(startLogout())
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);