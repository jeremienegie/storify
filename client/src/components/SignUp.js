import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { startSingup } from '../actions/auth/signup';
import { resetError } from '../actions/auth/error';
import SocialLogin from './SocialLogin';

export class Signup extends React.Component {
  state = {
    firstname: '',
    lastname: '',
    email: '',
    password: '',
    error: ''
  }

  componentWillUnmount() {
    this.props.resetError();
  }

  onFirstnameChange = (e) => {
    const firstname = e.target.value;
    this.setState({ firstname })
  }

  onLastnameChange = (e) => {
    const lastname = e.target.value;
    this.setState({ lastname })
  }

  onEmailChange = (e) => {
    const email = e.target.value;
    this.setState({ email })
  }

  onPasswordChange = (e) => {
    const password = e.target.value;
    this.setState({ password })
  }

  onSubmit = (e) => {
    e.preventDefault();
    this.props.startSignup(this.state.firstname, this.state.lastname, this.state.email, this.state.password);
  }
  render() {
    return (
      <div className="form-container">
      <div className="content-container">
        <h3 className="title">Sign up</h3>
        <div className="form--content">
          {this.props.error && <p className="form--error">{this.props.error}</p>}
          <form className="form" onSubmit={this.onSubmit}>
            <input className="input" type="text" placeholder="Your firstname" value={this.state.firstname} onChange={this.onFirstnameChange}/>
            <input className="input" type="text" placeholder="Your lastname" value={this.state.lastname} onChange={this.onLastnameChange}/>
            <input className="input" type="text" placeholder="Email address" value={this.state.email} onChange={this.onEmailChange}/>
            <input className="input" type="password" placeholder="Choose a password" value={this.state.password} onChange={this.onPasswordChange}/>
            <button className="button button--secondary">Sign up</button>
          </form>
          <SocialLogin />
          <span className="form-container--text">Already a memeber?<Link to="/login" className="button button--secondary button--link-simple">Log in</Link></span>
        </div>
        </div>
    </div>
    )
  }
}

const mapStateToProps = ({ error }) => ({
  error: error.error
});

const mapDispatchToProps = (dispatch) => ({
  startSignup: (firstname, lastname, email, password) => dispatch(startSingup(firstname, lastname, email, password)),
  resetError: () => dispatch(resetError())
})

export default connect(mapStateToProps, mapDispatchToProps)(Signup);