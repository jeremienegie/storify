import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment'

import { fetchComments } from '../../actions/comments';

export class ShowComments extends React.Component {
  dispayComments = () => {
    const comments = this.props.fetchComments(this.props.storyId);
    if (!comments.length) {
      return <p className="no-comment">Say something about this story</p>;
    }

    return comments.map(comment => (
      <div className="comment" key={comment.id}>
        <p className="comment--body">{comment.comment}</p>
        <div className="story--misc">
          <p className="comment--name">{comment.name}</p>
          <span className="story--time">{moment(comment.createdAt).fromNow()}</span>
        </div>
        <hr />
      </div>
    ));
  }

  render() {
    return (
      <div>
        <h3>Comment</h3>
        {this.dispayComments()}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  comment: state.comments
});

const mapDispatchToProps = (dispatch) => ({
  fetchComments: (storyId) => dispatch(fetchComments(storyId))
});

export default connect(mapStateToProps, mapDispatchToProps)(ShowComments)