import React from 'react';
import { connect } from 'react-redux';

import { startAddComment } from '../../actions/comments';

export class CommentForm extends React.Component {
  state = {
    comment: '',
    name: '',
    email: ''
  }

  onCommentChange = (e) => {
    const comment = e.target.value;
    this.setState({ comment })
  }

  onNameChange = (e) => {
    const name = e.target.value;
    this.setState({ name });
  }

  onEmailChange = (e) => {
    const email = e.target.value;
    this.setState({ email });
  }

  onSubmit = (e) => {
    e.preventDefault();

    if (!this.state.comment.trim().length || !this.state.name.trim().length || !this.state.email.trim().length) {
      return console.log('All fileds are required');
    }

    this.props.startAddComment({
      storyId: this.props.storyId,
      name: this.state.name,
      email: this.state.email,
      comment: this.state.comment,
      createdAt: Date.now()
    });
    this.setState({ name: '' });
    this.setState({ email: '' });
    this.setState({ comment: '' });
  }

  render() {
    return (
      <div>
        <form className="form" onSubmit={this.onSubmit}>
          <input className="input" type="text" placeholder="Your name" value={this.state.name} onChange={this.onNameChange}/>
          <input className="input" type="text" placeholder="Your email" value={this.state.email} onChange={this.onEmailChange}/>
          <textarea className="textarea" type="text" placeholder="Say something about this story" value={this.state.comment} onChange={this.onCommentChange}></textarea>
          <button className="button">Add Comment</button>
      </form>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  startAddComment: (comment) => dispatch(startAddComment(comment))
});

export default connect(undefined, mapDispatchToProps)(CommentForm);