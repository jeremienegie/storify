import React from 'react';
import { connect } from 'react-redux';

import StoryForm from './StoryForm';
import { startEditStory } from '../actions/stories';

export class EditStory extends React.Component {
  onSubmit = (story) => {
    this.props.startEditStory(this.props.story.id, story);
    this.props.history.push(`/read/${this.props.story.id}`);
  }
  render() {
    return (
      <div>
        <h1>Editing: {this.props.story.title}</h1>
        <StoryForm story={this.props.story} onSubmit={this.onSubmit} />
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  story: state.stories.find(story => story.id === props.match.params.id)
});

const mapDispatchToProps = (dispatch) => ({
  startEditStory: (id, updates) => dispatch(startEditStory(id, updates))
})

export default connect(mapStateToProps, mapDispatchToProps)(EditStory);