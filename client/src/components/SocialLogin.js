import React from 'react';

export default () => (
  <div>
    <button className="button button--google"><i className="fab fa-google-plus-square"></i> Continue with Google</button>
    <button className="button button--facebook"><i className="fab fa-facebook-square"></i> Continue with Facebook</button>
  </div>
);