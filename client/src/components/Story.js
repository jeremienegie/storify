import React from 'react';
import { connect } from 'react-redux';

import StoryItem from './StoryItem';

export const Story = (props) => (
  <div>
    <div className="content-container">
  		{
  			props.stories.length === 0 ? (
  				<div className="nothing-to-show">
  					<p>Hey!!! why don't you publish a story to start</p>
  				</div>
  			) : (
  				props.stories.filter(story => {
  					return story.bookmarked !== true;
  				}).reverse().map(story => (
  					<StoryItem key={story.id} {...story} />
  				))
  			)
  		}
    </div>
	</div>
)

const mapStateToProps = (state) => ({
  stories: state.stories.filter(story => story.bookmarked !== true)
});

export default connect(mapStateToProps)(Story);