import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import StoryItem from './StoryItem';

export const BookmarkedStory = (props) => {
  return (
    <div>
      <div className="content-container">
        <Link to='/story'><i className="fas fa-arrow-left"></i></Link>
        {
          props.stories.length === 0 ? (
            <div>
              <p className="nothing-to-show">Ahah!! you don't have any story bookmarked</p>
            </div>
          ) : (
            props.stories.filter(story => {
              return story.bookmarked === true;
            }).map(story => (
              <StoryItem key={story.id} {...story} />
            ))
          )
        }
      </div>
	 </div>
  )
}

const mapStateToProps = (state) => ({
  stories: state.stories.filter(story => story.bookmarked === true)
});

export default connect(mapStateToProps)(BookmarkedStory);