import React, { Component } from 'react';

export default class StoryForm extends Component {
  state = {
    title: this.props.story ? this.props.story.title : '',
    story: this.props.story ? this.props.story.story : '',
    error: ''
  }

  onTitleChange = (e) => {
    const title = e.target.value;
    this.setState({ title });
  }

  onStoryChange = (e) => {
    const story = e.target.value;
    this.setState({ story });
  }

  onBookmark = (e) => {
    this.props.onSubmit({
      title: this.state.title,
      story: this.state.story,
      bookmarked: true
    });
  }

  onSubmit = (e) => {
    e.preventDefault();
    if (!this.state.title.trim().length || !this.state.story.trim().length) {
      return this.setState({ error: 'Please fill in all the field' });
    }
    this.setState({ error: '' })
    this.props.onSubmit({
      title: this.state.title,
      story: this.state.story,
      bookmarked: false
    });
  }

  render() {
    return (
      <form className="form" onSubmit={this.onSubmit}>
        {this.state.error && <p>{this.state.error}</p>}
        <input className="input" type="text" placeholder="Title" value={this.state.title} onChange={this.onTitleChange} />
        <textarea className="textarea" name="" id="textarea" cols="30" rows="10" placeholder="Your story" value={this.state.story} onChange={this.onStoryChange}></textarea>
        <div className="form__btn">
          <button className="button" type='submit' disabled={!this.state.title.trim().length || !this.state.story.trim().length}>Submit</button>
          <button className="button button--secondary" type='button' disabled={!this.state.title.trim().length && !this.state.story.trim().length} onClick={this.onBookmark}>Bookmark</button>
        </div>
      </form>
    );
  }
}