import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { startLogin } from '../actions/auth/signin';
import { resetError } from '../actions/auth/error';
import SocialLogin from './SocialLogin';

export class Signin extends React.Component {
  state = {
    email: '',
    password: '',
    loading: false
  }

  componentWillUnmount() {
    this.props.resetError();
  }

  onEmailChange = (e) => {
    const email = e.target.value;
    this.setState({ email });
  }

  onPasswordChange = (e) => {
    const password = e.target.value;
    this.setState({ password });
  }

  onSubmit = (e) => {
    e.preventDefault();
    this.setState({ loading: true });

    this.props.startLogin(this.state.email, this.state.password);
  }

  onLoading = () => {
    if (this.state.loading) {
      return <svg width="35px" height="35px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" className="lds-ellipsis" style={{animationPlayState: 'running', animationDelay: '0s', background: 'none'}}>{/*circle(cx="16",cy="50",r="10")*/}<circle cx={84} cy={50} r={0} fill="#0288d1" style={{animationPlayState: 'running', animationDelay: '0s'}}><animate attributeName="r" values="10;0;0;0;0" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="1s" repeatCount="indefinite" begin="0s" style={{animationPlayState: 'running', animationDelay: '0s'}} /><animate attributeName="cx" values="84;84;84;84;84" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="1s" repeatCount="indefinite" begin="0s" style={{animationPlayState: 'running', animationDelay: '0s'}} /></circle><circle cx={84} cy={50} r="0.708846" fill="#0288d1" style={{animationPlayState: 'running', animationDelay: '0s'}}><animate attributeName="r" values="0;10;10;10;0" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="1s" repeatCount="indefinite" begin="-0.5s" style={{animationPlayState: 'running', animationDelay: '0s'}} /><animate attributeName="cx" values="16;16;50;84;84" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="1s" repeatCount="indefinite" begin="-0.5s" style={{animationPlayState: 'running', animationDelay: '0s'}} /></circle><circle cx="81.5899" cy={50} r={10} fill="#0288d1" style={{animationPlayState: 'running', animationDelay: '0s'}}><animate attributeName="r" values="0;10;10;10;0" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="1s" repeatCount="indefinite" begin="-0.25s" style={{animationPlayState: 'running', animationDelay: '0s'}} /><animate attributeName="cx" values="16;16;50;84;84" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="1s" repeatCount="indefinite" begin="-0.25s" style={{animationPlayState: 'running', animationDelay: '0s'}} /></circle><circle cx="47.5899" cy={50} r={10} fill="#0288d1" style={{animationPlayState: 'running', animationDelay: '0s'}}><animate attributeName="r" values="0;10;10;10;0" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="1s" repeatCount="indefinite" begin="0s" style={{animationPlayState: 'running', animationDelay: '0s'}} /><animate attributeName="cx" values="16;16;50;84;84" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="1s" repeatCount="indefinite" begin="0s" style={{animationPlayState: 'running', animationDelay: '0s'}} /></circle><circle cx={16} cy={50} r="9.29115" fill="#0288d1" style={{animationPlayState: 'running', animationDelay: '0s'}}><animate attributeName="r" values="0;0;10;10;10" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="1s" repeatCount="indefinite" begin="0s" style={{animationPlayState: 'running', animationDelay: '0s'}} /><animate attributeName="cx" values="16;16;16;50;84" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="1s" repeatCount="indefinite" begin="0s" style={{animationPlayState: 'running', animationDelay: '0s'}} /></circle></svg>
    }
    return <span>Log in</span>
  }

  render() {
    return (
      <div className="form-container">
        <div className="content-container">
          <h3 className="title">Log in</h3>
          <div className="form--content">
            {this.props.error && <p className="form--error">{this.props.error}</p>}
            <form className="form" onSubmit={this.onSubmit}>
              <input className="input" type="text" placeholder="Username or email" value={this.state.email} onChange={this.onEmailChange}/>
              <input className="input" type="password" placeholder="Password" value={this.state.password} onChange={this.onPasswordChange}/>
              <button disabled={this.state.loading} className="button button--secondary">{this.onLoading()}</button>
            </form>
            <SocialLogin />
            <span className="form-container--text">Not a memeber yet?<Link to="/signup" className="button button--secondary button--link-simple">Sign up</Link></span>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ error }) => ({
  error: error.error
});

const mapDispatchToProps = (dispatch) => ({
  startLogin: (email, password) => dispatch(startLogin(email, password)),
  resetError: () => dispatch(resetError())
});

export default connect(mapStateToProps, mapDispatchToProps)(Signin);