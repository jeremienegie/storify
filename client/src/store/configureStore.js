import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import storiesReducers from '../reducers/stories';
import commentReducers from '../reducers/comments';
import authReducers from '../reducers/auth/auth';
import errorReducers from '../reducers/auth/error';

export default () => {
  const store = createStore(combineReducers({
      stories: storiesReducers,
      comments: commentReducers,
      auth: authReducers,
      error: errorReducers
    }),
    applyMiddleware(thunk)
  )

  return store;
}