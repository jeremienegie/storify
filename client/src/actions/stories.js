import uuid from 'uuid';

// ADD_STORY
const addStory = (story) => ({
  type: 'ADD_STORY',
  story
});

export const startAddStory = (storyData = {}) => {
  const {
    id = uuid(),
      title = '',
      story = '',
      createdAt = 0,
      bookmarked = false
  } = storyData;

  const storyObj = { id, title, story, createdAt, bookmarked };
  return (dispatch) => {
    dispatch(addStory(storyObj));
  }
}

// FETCH_STORY
export const startFetchStory = (id) => {
  return (dispatch, getState) => {
    return getState().stories.filter((story) => story.id === id)
  }
}

// EDIT_STORY
const editStory = (id, updates) => ({
  type: 'EDIT_STORY',
  id,
  updates
});

export const startEditStory = (id, updates) => {
  return (dispatch) => {
    dispatch(editStory(id, updates));
  }
}

// REMOVE_STORY
const removeStory = ({ id } = {}) => ({
  type: 'REMOVE_STORY',
  id
});

export const startRemoveStory = ({ id } = {}) => {
  return (dispatch) => {
    dispatch(removeStory({ id }))
  }
}