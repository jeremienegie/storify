export const error = (errorCode) => ({
  type: errorCode
});

export const resetError = () => {
  return (dispatch) => {
    dispatch(error('RESET_ERROR'))
  }
}

export default error;