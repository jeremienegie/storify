import database, { firebase } from '../../firebase/firebase';

import errorMessage from './error';

export const startSingup = (firstname, lastname, email, password) => {
  return (dispatch) => {
    const userFullName = {
      firstname,
      lastname
    }
    return firebase.auth().createUserWithEmailAndPassword(email, password).then(user => {
      database.ref(`users/${user.uid}/fullName`).set(userFullName);
    }).catch(error => {
      dispatch(errorMessage(error.code));
    });
  }
}