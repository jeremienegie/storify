import { firebase } from '../../firebase/firebase';

import errorMessage from './error';

export const startLogin = (email, password) => {
  return (dispatch) => firebase.auth().signInWithEmailAndPassword(email, password).catch(error => {
    dispatch(errorMessage(error.code));
  });
}

export const startLogout = () => () => firebase.auth().signOut();