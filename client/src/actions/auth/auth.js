import database from '../../firebase/firebase';

export const login = (uid, userInfo) => ({
  type: 'LOGIN',
  uid,
  userInfo
});

export const logout = () => ({
  type: 'LOGOUT'
});

export const startLogin = (uid) => {
  return (dispatch) => {
    return database.ref(`users/${uid}/fullName`).once('value', (snapshot) => {
      dispatch(login(uid, snapshot.val()));
    });
  }
}