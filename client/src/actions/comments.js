import uuid from 'uuid';

//ADD_COMMENT
const addComment = (comment) => ({
  type: 'ADD_COMMENT',
  comment: {
    id: uuid(),
    ...comment
  }
});

export const startAddComment = (comment) => {
  return (dispatch) => {
    dispatch(addComment(comment))
  }
}

export const fetchComments = (storyId) => {
  return (dispatch, getState) => {
    return getState().comments.filter(comment => comment.storyId === storyId);
  }
}