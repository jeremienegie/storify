import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

export const PublicRoute = ({
  isAuthenticated,
  component: Component,
  ...rest
}) => {
  const component = (props) => (
    isAuthenticated ? (
      <Redirect to="/" />
    ) : (
      <Component {...props} />
    )
  );

  return <Route {...rest} component={component} />
}

const mapStateToProps = (state) => ({
  isAuthenticated: !!state.auth.uid
})

export default connect(mapStateToProps)(PublicRoute);