import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';

import Header from '../components/Header';
import Landing from '../components/Landing';
import NewStory from '../components/NewStory';
import ReadStory from '../components/ReadStory';
import EditStory from '../components/EditStory';
import BookmarkedStory from '../components/BookmarkedStory';
import Login from '../components/Login';
import Signup from '../components/Signup';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';
import NotFound from '../components/NotFound';

export const history = createHistory();

export default () => (
  <Router history={history}>
    <div>
      <Header />
      <Switch>
        <Route path="/" component={Landing} exact/>
        <PrivateRoute path="/new" component={NewStory} />
        <PrivateRoute path="/edit/:id" component={EditStory} />
        <PrivateRoute path="/bookmarks" component={BookmarkedStory} />
        <PublicRoute path="/login" component={Login} />
        <PublicRoute path="/signup" component={Signup} />
        <Route path="/read/:id" component={ReadStory} />
        <Route component={NotFound} />
      </Switch>
    </div>
  </Router>
);