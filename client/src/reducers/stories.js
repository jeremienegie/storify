const defaultState = [];

export default (state = defaultState, action) => {
  switch (action.type) {
    case 'ADD_STORY':
      return [
        ...state,
        action.story
      ];
    case 'EDIT_STORY':
      return state.map(story => {
        if (story.id === action.id) {
          return {
            ...story,
            ...action.updates
          }
        }
        return story
      });
    case 'REMOVE_STORY':
      return state.filter(({ id }) => action.id !== id);
    default:
      return state;
  }
}