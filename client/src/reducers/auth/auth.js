export default (state = {}, action) => {
  switch (action.type) {
    case 'LOGIN':
      return { uid: action.uid, userInfo: action.userInfo };
    case 'LOGOUT':
      return {};
    default:
      return state;
  }
}