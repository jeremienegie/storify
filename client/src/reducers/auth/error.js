export default (state = {}, action) => {
  switch (action.type) {
    case 'auth/invalid-email':
      return { error: 'Please provide a valid email address' };
    case 'auth/wrong-password':
      return { error: 'That password is not correct' };
    case 'auth/user-not-found':
      return { error: 'There is no account assicated with those credentials' };
    case 'auth/weak-password':
      return { error: 'The password must contain at least six character' };
    case 'RESET_ERROR':
      return {};
    default:
      return state;
  }
}