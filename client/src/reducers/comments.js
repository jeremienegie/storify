const defaultState = [];

export default (state = defaultState, action) => {
  switch (action.type) {
    case 'ADD_COMMENT':
      return [
        ...state,
        action.comment
      ]
    default:
      return state
  }
}