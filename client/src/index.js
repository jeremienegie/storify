import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import registerServiceWorker from './registerServiceWorker';
import { firebase } from './firebase/firebase';

import configureStore from './store/configureStore';
import AppRouter from './routes/AppRouter';
import { startLogin, logout } from './actions/auth/auth';

import 'normalize.css/normalize.css';
import './styles/style.css';

const store = configureStore();
const jsx = (
  <Provider store={store}>
    <AppRouter />
  </Provider>
);

firebase.auth().onAuthStateChanged(user => {
  if (user) {
    store.dispatch(startLogin(user.uid)).then(() => {
      ReactDOM.render(jsx, document.getElementById('root'));
    });
  } else {
    store.dispatch(logout());
    ReactDOM.render(jsx, document.getElementById('root'));
  }
});

registerServiceWorker();